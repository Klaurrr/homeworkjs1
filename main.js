//#1

const arr = [1, 2, 3, 5, 1, 6, 2, 5, 6, 7];

const resultFirst = Array.from(new Set(arr));

console.log(resultFirst)


//#2 

const objOne = {
    id: 1,
    name: 'UserName'
};

const objTwo = {
    id: 1,
    password: 'Qwerty'
}

const resultSecond = Object.assign(objOne, objTwo)

console.log(resultSecond);

//#3

const add = (x, y) => x + y

//#4

const delay = ms => {
    return new Promise(resolve => {
        resolve(setTimeout(() => {
            console.log(`Выполнилось через ${ms / 1000} сек`)
        }, ms))
    })
}

delay(2000);





